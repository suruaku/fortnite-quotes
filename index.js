const quotes = [
  "People with scars are always the strongest.",
  "If someone else can edit it, it was never your wall to begin with.",
  "You don't get materials from breaking other people's builds.",
  "You might change the skin but at the end of the day you're still the same player.",
  "Even with bad loot you still try to go for the win.",
  "You can get knocked but with the right teammates you can get back up.",
  "Even if you're one hp, you can still do max damage.",
  "If you weren't there when I was pushing the squad, don't be there when I'm picking up the loot.",
  "Players will help you at your highest then leave when you're at your lowest.",
  "Playing solo is the scariest, winning a solo is the hardest but winning the solo feels the best.",
  "Your circle will get smaller the closer you are to winning.",
  "Even winners get sent back to the lobby.",
  "It's only when the map changes that people care about the old one.",
  "Not only your enemies can shoot you down your sky base but your teammates can too.",
  "The new Fortnite update is like a relationship, you can bo back to what you left but it still won't be the same as before.",
  "Just because you're in the storm doesn't mean the game is over.",
  "There might be a squad leader but everyone work together to win.",
  "Life is like a skybase, everyone wants to bring you down when your at the top.",
  "Unopened chest shine the brightest.",
  "If she was never there with you in the storm, why bring her to the final circle?",
  "Metal takes longer to build, but in the end it's always the strongest.",
  "The smaller the circle, the better the people.",
  "Some people start on highground, others have to build up to it.",
  "It doesn't matter if you have a gold scar or a gray AR, you will still win if you're better.",
  "If somebody closes a door, somewhere else one will open.",
  "If you want to get into the vault, you'll first have to find the key.",
  "Remember, if you're downed, someone else will get you up.",
  "Remember, if you kill someone, you'll send them to the lobby, but if you die, you'll be sent to the lobby yourself."
];

const getRandomQuote = () => quotes[ Math.floor( Math.random() * quotes.length )];

const getAllQuotes = () => quotes;

export { getRandomQuote, getAllQuotes };
