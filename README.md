# fortnite-quotes

Fornite-quotes is a free and open-source library with an intuitive way to give your devs some motivation.

Tired of depressive error messages? Replace them with a motivational quote from `fortnite-quotes`!
Because who needs a boring "SyntaxError" at the time when your production breaks
when you can have a quote like, "Even winners get sent back to the lobby."

## Getting Started

Use the following command to install the package:
```bash
npm install fortnite-quotes
```

Example:

```javascript
import { getRandomQuote } from "fortnite-quotes";

try {
  throw new Error( getRandomQuote());
} catch( err ) {
  console.error( err );
}
```

## License

[MIT](./LICENSE)
